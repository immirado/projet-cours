<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="container-fluid">
    <a href="#list-annonce" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                  default="Skip to content&hellip;"/></a>

        <h2 class="card-title"><g:message code="default.list.label" args="[entityName]"/></h2>


    <div class="nav mt-2" role="navigation">
        <ul>
            <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
            <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                                  args="[entityName]"/></g:link></li>
        </ul>
    </div>

    <div id="list-annonce" class="d-flex flex-column" role="main">
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>


        <table class="table table-bordered" width="100" cellspacing="0">
            <thead>
            <tr>
                <th>Title</th>
                <th>Description</th>
                <th>Price</th>
                <th>Date created</th>
                <th>Last updated</th>
                <th>Illustrations</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${annonceList}" var="annonce">
                <tr>
                    <td><a href="/annonce/${annonce.id}">${annonce.title}</a></td>
                    <td>${annonce.description}</td>
                    <td>${annonce.price}</td>
                    <td>${annonce.dateCreated}</td>
                    <td>${annonce.lastUpdated}</td>
                    <td>
                    <div class="row">
                        <g:each in="${annonce.illustrations}" var="illustration">
                                <div class="col-md-2">
                                    <img src="${resource(dir: 'images', file: '' + illustration.filename)}" width="30" class="img-responsive"/>
                                </div>
                        </g:each>
                    </div>
                    </td>
                </tr>
            </g:each>
            </tbody>
        </table>

        <div class="pagination">
            <g:paginate total="${annonceCount ?: 0}" action="search" params="${params}"/>
        </div>
    </div>
</div>
</body>
</html>