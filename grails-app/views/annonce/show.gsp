<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<a href="#show-annonce" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                              default="Skip to content&hellip;"/></a>

<div class="nav ml-3" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label"
                                                           args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="show-annonce" class="container-fluid" role="main">
    <h3><g:message code="default.show.label" args="[entityName]"/></h3><hr>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:form resource="${this.annonce}" method="PUT">
        <div class="row">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12 mb-3">
                    <label>Title :</label>

                    <p style="display:inline">${this.annonce.title}</p>
                </div>

                <div class="col-md-12 mb-3">
                    <label>Description :</label>

                    <p style="display:inline">${this.annonce.description}</p>
                </div>

                <div class="col-md-12 mb-3">
                    <label>Price :</label>

                    <p style="display:inline">${this.annonce.price}</p>
                </div>

                <div class="col-md-12 mb-3">
                    <label>Author :</label>

                    <a href="/user/show/${this.annonce.author.id}">${this.annonce.author.username}</a>
                </div>
            </div>
        </div>

        <div class="col-md-8">

            <div class="row">
                <div class="mb-3">
                    <label class="mb-3">Illustrations</label><br>
                    <g:each in="${this.annonce.illustrations}" var="illustration">
                        <img class="mr-4" src="${resource(dir: 'images', file: '' + illustration.filename)}" alt="${illustration.filename}"/>
                    </g:each>
                </div>
            </div>
        </div>
        </div>
    </g:form>
    <g:form resource="${this.annonce}" method="DELETE">
        <fieldset class="buttons">
            <g:link class="edit" action="edit" resource="${this.annonce}"><g:message
                    code="default.button.edit.label"
                    default="Edit"/></g:link>
            <input class="delete" type="submit"
                   value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                   onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
