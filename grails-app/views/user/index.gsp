<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#list-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>

        <ul id="list-user" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>

            <table class="table table-bordered" width="100" cellspacing="0">
                <thead>
                <tr>
                    <th>Username</th>
                    <th>Annonces</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${userList}" var="user">
                    <tr>
                        <td><a href="user/show/${user.id}">${user.username}</a></td>
                        <td>
                            <ul>
                                <g:each in="${user.annonces}" var="annonce">
                                        <li><a href="annonce/${annonce.id}">
                                            ${annonce.title}
                                        </a> </li>
                                </g:each>
                            </ul>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>

            <div class="pagination">
                <g:paginate total="${userCount ?: 0}" />
            </div>
        </div>
    </body>
</html>