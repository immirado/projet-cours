<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>Se connecter</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>

    <asset:stylesheet src="fontawesome-free/css/all.min.css"/>
    <asset:stylesheet
            src="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"/>
    <asset:stylesheet src="application.css"/>
    <asset:stylesheet src="sb-admin-2.css"/>

    <g:layoutHead/>
</head>

<body class="pg-gradient-primary">
<div class="container">

    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">
            <div class="card-body p-0">
                <g:layoutBody/>
            </div>

        </div>
    </div>
    <asset:javascript src="jquery.min.js"/>
    <asset:javascript src="bootstrap.bundle.js"/>
    <asset:javascript src="jquery.easing.js"/>
    <asset:javascript src="sb-admin-2.js"/>
</div>
</body>
</html>
