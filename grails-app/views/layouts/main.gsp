<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
    <g:layoutTitle default="Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>

    <asset:stylesheet src="fontawesome-free/css/all.min.css"/>
    <asset:stylesheet
            src="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"/>
    <asset:stylesheet src="application.css"/>
    <asset:stylesheet src="sb-admin-2.css"/>

    <g:layoutHead/>
</head>

<body class="page-top">
<div id="wrapper">
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
            <div class="sidebar-brand-icon rotate-n-15">
                <i class="fas fa-ad"></i>
            </div>

            <div class="sidebar-brand-text mx-3">Lecoincoin <sup>Groupe 5</sup></div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">


        <li class="nav-item active">
            <a class="nav-link" href="/annonce">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Annonces</span></a>
        </li>

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="/users">
                <i class="fas fa-fw fa-user"></i>
                <span>Utilisateurs</span></a>
        </li>
    </ul>

    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>

                <form class="form-inline" action="/annonce/search">
                    <div class="form-group mx-sm-3 mb-2">
                        <label for="search_query" class="sr-only">Rechercher une annonce ...</label>
                        <input name="search_query" type="text" class="form-search form-control bg-light border-0 small" id="search_query" value="${search_query}" placeholder="Rechercher une annonce ...">
                    </div>
                </form>

                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <!-- Nav Item - Search Dropdown (Visible Only XS) -->

                    <!-- Nav Item - Alerts -->

                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-user"></i>
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small">&nbsp;${session.user?.username}</span>

                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                             aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="/logout">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                Logout
                            </a>
                        </div>
                    </li>

                </ul>

            </nav>

            <g:layoutBody/>

            <div class="myfooter" role="contentinfo">TP Grails - MBDS Madagascar 2020-2021 - Groupe 5</div>

            <div id="spinner" class="spinner" style="display:none;">
                <g:message code="spinner.alt" default="Loading&hellip;"/>
            </div>

        </div>
    </div>
    <asset:javascript src="jquery.min.js"/>
    <asset:javascript src="bootstrap.bundle.min.js"/>
    <asset:javascript src="jquery.easing.js"/>
    <asset:javascript src="sb-admin-2.js"/>
</div>
</body>
</html>
