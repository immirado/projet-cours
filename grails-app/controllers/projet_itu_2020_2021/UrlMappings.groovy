package projet_itu_2020_2021

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: "annonce", action: "index")
        "500"(view:'/error')
        "404"(view:'/notFound')
        "/annonces"(controller: "annonce", action: "index")
        "/annonce/$id"(controller: "annonce", action: "show")
        "/users"(controller: "user", action: "index")
        "/annonce/create"(controller: "annonce", action: "create")
        "/annonce/save"(controller: "annonce", action: "save")
        "/logout"(controller: "user", "action":"logout")
        "/annonce/search"(controller: "annonce", action: "search")
    }
}
