package com.mbds.grails

import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured

import javax.servlet.http.HttpServletResponse

@Secured('ROLE_ADMIN')
class ApiController {
    AnnonceService annonceService
    UserService userService
    SpringSecurityService springSecurityService
//    GET / PUT / PATCH / DELETE
//    url : localhost:8081/projet/api/annonce(s)/{id}
    def annonce() {
        switch (request.getMethod()) {
            case "GET":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def annonceInstance = Annonce.get(params.id)
                if (!annonceInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND

                return serializeData(annonceInstance, request.getHeader("Accept"))
                break

            case "PUT":
                def req_json = request.getJSON()

                if (!req_json.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def annonceInstance = Annonce.get(req_json.id)

                if (!annonceInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                annonceInstance.title = req_json.title
                annonceInstance.description = req_json.description
                annonceInstance.price = Double.parseDouble("" + req_json.price)

                annonceService.save(annonceInstance)
                def updatedAnnonce = Annonce.get(req_json.id)
                return serializeData(updatedAnnonce, request.getHeader("Accept"))

                break

            case "PATCH":
                def req_json = request.getJSON()

                if (!req_json.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def annonceInstance = Annonce.get(req_json.id)

                if (!annonceInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                annonceInstance.title = req_json.title ? req_json.title : annonceInstance.title
                annonceInstance.description = req_json.description ? req_json.description : annonceInstance.description
                annonceInstance.price = req_json.price ? Double.parseDouble("" + req_json.price) : annonceInstance.price

                annonceService.save(annonceInstance)
                def updatedAnnonce = Annonce.get(req_json.id)

                return serializeData(updatedAnnonce, request.getHeader("Accept"))

                break
            case "DELETE":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def annonceInstance = Annonce.get(params.id)
                if (!annonceInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                annonceInstance.delete(flush: true)
                return response.status = HttpServletResponse.SC_OK
                break
            default:
                return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                break
        }
        return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
    }

//    GET / POST
    def annonces() {
        switch (request.getMethod()) {
            case "GET":
                def limit = params.limit ? params.limit : 10
                def offset = params.offset ? params.offset :0
                def listAnnonce = Annonce.findAll([max: limit, offset: offset])
                if (!listAnnonce)
                    return response.status = HttpServletResponse.SC_NOT_FOUND

                return serializeData(listAnnonce, request.getHeader("Accept"))
                break
            case "POST":
                def req_json = request.getJSON()

                def annonce = new Annonce()
                annonce.title = req_json.title
                annonce.description = req_json.description
                annonce.price = Double.parseDouble("" + req_json.price)
                annonce.author = (User) springSecurityService.currentUser

                annonceService.save(annonce)
                return serializeData(annonce, request.getHeader("Accept"))
                break
        }
    }

//    GET / PUT / PATCH / DELETE
    def user() {
        switch (request.getMethod()) {
            case "GET":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def userInstance = User.get(params.id)
                if (!userInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND

                return serializeData(userInstance, request.getHeader("Accept"))
                break

            case "PUT":
                def req_json = request.getJSON()

                if (!req_json.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def userInstance = User.get(req_json.id)

                if (!userInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                userInstance.username = req_json.username
                userInstance.password = req_json.password


                userService.save(userInstance)

                def updatedUser = User.get(req_json.id)
                return serializeData(updatedUser, request.getHeader("Accept"))
                break
            case "PATCH":
                def req_json = request.getJSON()

                if (!req_json.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def userInstance = User.get(req_json.id)

                if (!userInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                userInstance.username = req_json.username ? req_json.username : userInstance.username
                userInstance.password = req_json.password ? req_json.password : userInstance.password


                userService.save(userInstance)
                def updatedUser = User.get(req_json.id)
                return serializeData(updatedUser, request.getHeader("Accept"))
                break
            case "DELETE":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def userInstance = User.get(params.id)
                if (!userInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                UserRole.removeAll(userInstance)
                userInstance.delete(flush: true)
                return response.status = HttpServletResponse.SC_OK
                break
            default:
                return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                break
        }
        return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
    }

//    GET / POST
    def users() {
        switch (request.getMethod()) {
            case "GET":
                def limit = params.limit ? params.limit : 10
                def offset = params.offset ? params.offset :0
                def listUsers = User.findAll([max: limit, offset: offset])
                if (!listUsers)
                    return response.status = HttpServletResponse.SC_NOT_FOUND

                return serializeData(listUsers, request.getHeader("Accept"))
                break

            case "POST":
                def req_json = request.getJSON()

                def user = new User()
                user.username = req_json.username
                user.password = req_json.password

                userService.save(user)
                return serializeData(user, request.getHeader("Accept"))
                break
        }
    }

    def serializeData(object, format)
    {
        switch (format)
        {
            case 'json':
            case 'application/json':
            case 'text/json':
                render object as JSON
                break
            case 'xml':
            case 'application/xml':
            case 'text/xml':
                render object as XML
                break
            default:
                render object as JSON
                break
        }
    }
}
