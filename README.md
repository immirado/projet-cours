**Lecoincoin - Groupe 5**

Lecoincoin est une application pour gérer des annonces

**Back Office:**

Pagination de toutes les listes

- Gestion des utilisateurs
    - Liste des utilisateurs
    - Modification des utilisateurs
    - Création d'un nouvel utilisateur
    - Suppresion d'un utilisateur


- Gestion des annonces
    - Liste des annonces
    - Modification d'une annonce
    - Création d'une nouvelle annonce
    - Suppresion d'une annonce


- Login
    - Gestion des rôles et permissions des utilisateurs:
        - Admin: création, modification, suppression d'une annonce ou d'un utilisateur
        - Modérateur: modification d'une annonce ou d'un utilisateur
        - Client: peut seulement voir la liste des utilisateurs ou des annonces


    - Accès (Username/Password):
        - Admin: admin/password
        - Modérateur: moderateur/password 
        - Client: client/password


    - Les pages de Lecoincoin ne sont pas visibles aux utilisateurs non authentifiés


- Recherche paginée d'une annonce

**API Rest**

    - Liste des utilisateurs
    - Modification des utilisateurs
    - Création d'un nouvel utilisateur
    - Suppresion d'un utilisateur

    - Liste des annonces
    - Modification d'une annonce
    - Création d'une nouvelle annonce
    - Suppresion d'une annonce

    - Login

    Les données en réponse peuvent être données sous forme JSON ou XML

**Base de données:**
Nous avons utilisé Mysql pour la persistence des données. On a utilisé les services de Alwaysdata.

**Collection Postman (pour la version locale):** https://www.getpostman.com/collections/60bcf173020a3a3f7f05

**Collection Postman (version déployée):** https://www.getpostman.com/collections/cf8a34bc3dd37639d86e

Lecoincoin a été **déployé** sur heroku et disponible à partir de ce lien: https://lecoincoin-groupe5.herokuapp.com/

Pour une utilisation locale, voici les instructions pour l'installation:
- Cloner ce repository: https://gitlab.com/immirado/projet-cours.git
- Ouvrir le projet dans votre IDE (IntelliJ)
- Lancer Gradle tasks
- Lancer le projet
- Lecoincoin s'ouvrira dans le navigateur


**Améliorations que l'on pourrait ajouter à l'avenir:**
- Ajouter un dashboard pour permettre aux utilisateurs d'avoir une vision globale sur les annonces et les utilisateurs de Lecoincoin

